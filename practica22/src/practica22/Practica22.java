package practica22;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author estefani 
 *
 */
public class Practica22 {

    static final int tamanyMaxCamp = 40;
    static final int tamanyMaxCampComentari = 140;
    static final String SI = "Sí";
    static final String NO = "No";
    static final String CAMP_EMPIEZA_MAJUSCULAS = "¿El campo empieza con una letra en majúsculas? ";
    static final String CAMP_SUPERA_NUM_CARACTERES = "¿El campo supera los 40 caracteres de largo? ";
    static final String CAMP_COMENTARI_SUPERA_NUM_CARACTERES = "¿El campo comentario supera los 140 caracteres de largo? ";
    static final String CAMP_EMAIL_FORMAT_VALID = "¿El campo email tiene un formato válido? ";
    static final String CAMP_URL_FORMAT_VALID = "¿El campo URL tiene un formato válido? ";
    static final String CAMP_PARAULES_PROHIBIDES = "¿El campo contiene alguna palabra prohibida?";

    private static void imprimeixResposta(boolean respostaAfirmativa) {
        if (respostaAfirmativa) {
            System.out.println(SI);
        } else {
            System.out.println(NO);
        }
    }

    public static void main(String[] args) {

        System.out.println("****Mis Pokemons**** ");
        Pokemon picachu = new Pokemon();
        picachu.setPok_id("picachuXIL");
        System.out.println(picachu);

        Pokemon katrina = new Pokemon("KatrinamMEn1", "katrina", "Grem", "Tinto", 2, 223, 500, 23.32, 2.2, 5.5, 4.5, 2.2);
        System.out.println(katrina);
        System.out.println("\n");

        ProvaValidador validadorEmpiezaMajuscula = new ProvaValidador();
        System.out.println(CAMP_EMPIEZA_MAJUSCULAS);
        imprimeixResposta(validadorEmpiezaMajuscula.validaPrimeraLetraMAjusculas("El pato"));
        System.out.println(CAMP_EMPIEZA_MAJUSCULAS);
        imprimeixResposta(validadorEmpiezaMajuscula.validaPrimeraLetraMAjusculas("el pato"));
        System.out.println("\n");

        System.out.println("Nombre");//nombre
        ProvaValidador validadorNumeroCaracteresNombre = new ProvaValidador();
        System.out.println(CAMP_SUPERA_NUM_CARACTERES);
        imprimeixResposta(validadorNumeroCaracteresNombre.validaTamanyMaximCamp("El pato", tamanyMaxCamp));

        System.out.println("Email");//email
        ProvaValidador validadorNumeroCaracteresEmail = new ProvaValidador();
        System.out.println(CAMP_SUPERA_NUM_CARACTERES);
        imprimeixResposta(validadorNumeroCaracteresEmail.validaTamanyMaximCamp("miquel.angel.amoros@gmail.com", tamanyMaxCamp));

        System.out.println("Sitio Web");//sitio web
        ProvaValidador validadorNumeroCaracteresSitioWeb = new ProvaValidador();
        System.out.println(CAMP_SUPERA_NUM_CARACTERES);
        imprimeixResposta(validadorNumeroCaracteresSitioWeb.validaTamanyMaximCamp("https://netbeans.apache.org/", tamanyMaxCamp));
        System.out.println("\n");

        System.out.println("Comentario");//comentari 
        ProvaValidador validadorNumeroCaracteresComentario = new ProvaValidador();
        System.out.println(CAMP_COMENTARI_SUPERA_NUM_CARACTERES);
        imprimeixResposta(validadorNumeroCaracteresComentario.validaTamanyMaximCamp("Pressuposem que els usuaris d’una aplicació web disposen del següent formulari on enviar comentaris.\n"
                + "Crea la classe “Validador” que implementi els mètodes que ens permetin comprovar el següent abans d’enviar un comentari", tamanyMaxCampComentari));
        System.out.println("\n");

        System.out.println("Email Format");//formato correcto del email 
        ProvaValidador validadorFormatEmail = new ProvaValidador();
        System.out.println(CAMP_EMAIL_FORMAT_VALID);
        imprimeixResposta(validadorFormatEmail.validaCampEmail("miquel.angel.amoros@gmail.com"));
        System.out.println("\n");

        /*No funciona
        System.out.println("URL Format");//formato correcto del email 
        ProvaValidador validadorFormatURL = new ProvaValidador();
        System.out.println(CAMP_URL_FORMAT_VALID);
        imprimeixResposta(validadorFormatURL.validaCampUrl("https://netbeans.apache.org/"));
         */
        System.out.println("\n");

        System.out.println("Paraules prohibides");
        ProvaValidador validadorParaulesProhibides = new ProvaValidador();
        System.out.println(CAMP_PARAULES_PROHIBIDES);
        imprimeixResposta(validadorParaulesProhibides.validaParaulesProhibides("anna ximple com imbècil sense sentit"));
        System.out.println(CAMP_PARAULES_PROHIBIDES);
        imprimeixResposta(validadorParaulesProhibides.validaParaulesProhibides("anna te sentit"));
        System.out.println(CAMP_PARAULES_PROHIBIDES);
        imprimeixResposta(validadorParaulesProhibides.validaParaulesProhibides("imbecil  ets tu"));
        System.out.println(CAMP_PARAULES_PROHIBIDES);
        imprimeixResposta(validadorParaulesProhibides.validaParaulesProhibides("TU ets IMBECIL"));
        System.out.println(CAMP_PARAULES_PROHIBIDES);
        imprimeixResposta(validadorParaulesProhibides.validaParaulesProhibides("Ets un burro SUPREMACISTA"));
        System.out.println("\n");

        ArrayList<ComentarisUsuaris> comentarisArrayList = new ArrayList<ComentarisUsuaris>();

        ComentarisUsuaris comentari1 = new ComentarisUsuaris("Sofia", "sofia@gmail.com", "sofia.com", "que mierda");
        comentarisArrayList.add(comentari1);
        ComentarisUsuaris comentari2 = new ComentarisUsuaris("erre", "eee@gmail.com", "eee.com", "que has hcho");
        comentarisArrayList.add(comentari2);
        ComentarisUsuaris comentari3 = new ComentarisUsuaris("elel", "dsd@gmail.com", "gf.com", "no hables más");
        comentarisArrayList.add(comentari3);
        ComentarisUsuaris comentari4 = new ComentarisUsuaris("Sese", "s@gmail.com", "sewsn.com", "tengo ganas de dormir");
        comentarisArrayList.add(comentari4);

      
        //listar comentarios
        System.out.println("LISTAR COMENTARIOS");
         ComentarisUsuaris.listarComentarios(comentarisArrayList);

        //comentario por nombre 
        System.out.println("ENCONTRAR COMENTARIO POR NOMBRE");
        String nombre = "Sese";
        ComentarisUsuaris.encontrarComentarioPorNombre(nombre,comentarisArrayList);
        
        
        System.out.println("USO HashMap");
        HashMap Comentari5 = new HashMap();

        Comentari5.put("nom", "sofi");
        Comentari5.put("email", "Berlin@gmail.com");
        Comentari5.put("web", "Oslo.com");
        Comentari5.put("comentari", "Washington DC es precioso");
        // listar
        for (Object i : Comentari5.keySet()) {
            System.out.println(i);
        }
        for (Object i : Comentari5.keySet()) {
            System.out.println("Key: " + i + " Value: " + Comentari5.get(i));
        }

    }

    

}
