package practica22;

import java.util.Objects;

/**
 *
 * @author estefani
 */
public class Pokemon {

    private String Pok_id;
    private String nombre;
    private String tipo1;
    private String tipo2;
    private int nivel;
    private int energiaAct;
    private int energiaMax;
    private double ataque;
    private double defensa;
    private double velocidad;
    private double defensaEsp;
    private double ataqueEsp;

    //constructors
    public Pokemon() {
    }

    public Pokemon(String Pok_id, String nombre) {
        this.Pok_id = Pok_id;
        this.nombre = nombre;
    }

    public Pokemon(String Pok_id, String nombre, String tipo1, String tipo2, int nivel, int energiaAct, int energiaMax, double ataque, double defensa, double velocidad, double defensaEsp, double ataqueEsp) {
        this.Pok_id = Pok_id;
        this.nombre = nombre;
        this.tipo1 = tipo1;
        this.tipo2 = tipo2;
        this.nivel = nivel;
        this.energiaAct = energiaAct;
        this.energiaMax = energiaMax;
        this.ataque = ataque;
        this.defensa = defensa;
        this.velocidad = velocidad;
        this.defensaEsp = defensaEsp;
        this.ataqueEsp = ataqueEsp;
    }

    
    //getters i setters
    public String getPok_id() {
        return Pok_id;
    }

    public void setPok_id(String Pok_id) {
        this.Pok_id = Pok_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo1() {
        return tipo1;
    }

    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }

    public String getTipo2() {
        return tipo2;
    }

    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getEnergiaAct() {
        return energiaAct;
    }

    public void setEnergiaAct(int energiaAct) {
        this.energiaAct = energiaAct;
    }

    public int getEnergiaMax() {
        return energiaMax;
    }

    public void setEnergiaMax(int energiaMax) {
        this.energiaMax = energiaMax;
    }

    public double getAtaque() {
        return ataque;
    }

    public void setAtaque(double ataque) {
        this.ataque = ataque;
    }

    public double getDefensa() {
        return defensa;
    }

    public void setDefensa(double defensa) {
        this.defensa = defensa;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public double getDefensaEsp() {
        return defensaEsp;
    }

    public void setDefensaEsp(double defensaEsp) {
        this.defensaEsp = defensaEsp;
    }

    public double getAtaqueEsp() {
        return ataqueEsp;
    }

    public void setAtaqueEsp(double ataqueEsp) {
        this.ataqueEsp = ataqueEsp;
    }

    //override
    @Override
    public String toString() {
        return "Pokemon{" + "Pok_id=" + Pok_id + ", nombre=" + nombre + ", tipo1=" + tipo1 + ", tipo2=" + tipo2 + ", nivel=" + nivel + ", energiaAct=" + energiaAct + ", energiaMax=" + energiaMax + ", ataque=" + ataque + ", defensa=" + defensa + ", velocidad=" + velocidad + ", defensaEsp=" + defensaEsp + ", ataqueEsp=" + ataqueEsp + '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pokemon other = (Pokemon) obj;
        if (!Objects.equals(this.Pok_id, other.Pok_id)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }

   
}
