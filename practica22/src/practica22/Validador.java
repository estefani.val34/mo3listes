package practica22;

/**
 * MIMAR EL DE PERSONAS DE LA SSESSIO 7
 *
 * @author estefani
 */
public class Validador {

    public Validador() {
    }

    //METODOS
    
    /**
     * Validar que cap dels camps de text requerits (inclòs el camp de
     * comentaris) sigui correcte.
     *
     * @param camp
     * @return
     */
    public boolean validaCampNoBuit(String camp) {
        return camp != null && camp.isEmpty();
    }

    /**
     * Validar que els camps de texto 1,2 i 3 
     * no superin els 40 caràcters de llarg.
     * @param camp
     * @param tamanyMaxCamp
     * @return 
     */
    public boolean validaTamanyMaximCamp(String camp, int tamanyMaxCamp) {
        return camp.length() > tamanyMaxCamp;
    }
    
    

}
