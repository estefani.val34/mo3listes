package Ex2;
import java.util.ArrayList;
/**
 *
 * @author Francisco Regaña Corral
 */
public class Pokemon {
    
    //Atributs:
    private String Pok_id;
    private String nombre;
    private String tipo1;
    private String tipo2;
    private int nivel;
    private int energiaAct;
    private int energiaMax;
    private double ataque;
    private double defensa;
    private double velocidad;
    private double defensaEsp;
    private double ataqueEsp;

    /**
     * Constructor que crea un nou objecte amb tots els atributs
     */
    public Pokemon(String Pok_id, String nombre, String tipo1, String tipo2, int nivel, int energiaAct, int energiaMax, double ataque, double defensa, double velocidad, double defensaEsp, double ataqueEsp) {
        this.Pok_id = Pok_id;
        this.nombre = nombre;
        this.tipo1 = tipo1;
        this.tipo2 = tipo2;
        this.nivel = nivel;
        this.energiaAct = energiaAct;
        this.energiaMax = energiaMax;
        this.ataque = ataque;
        this.defensa = defensa;
        this.velocidad = velocidad;
        this.defensaEsp = defensaEsp;
        this.ataqueEsp = ataqueEsp;
    }

    public Pokemon() {
    }
    
    /**
     * Constructor que crea un nou objecte pokemon a partir de les dades d'un altre 
     * pokemon.
     * @param pk Pokemon del que volem extrure les dades 
     */
    public Pokemon(Pokemon pk){
        this.Pok_id = pk.Pok_id;
        this.ataque = pk.ataque;
        this.ataqueEsp = pk.ataqueEsp;
        this.defensa = pk.defensa;
        this.defensaEsp = pk.defensaEsp;
        this.energiaAct = pk.energiaAct;
        this.energiaMax = pk.energiaMax;
        this.nivel = pk.nivel;
        this.nombre = pk.nombre;
        this.tipo1 = pk.tipo1;
        this.tipo2 = pk.tipo2;
        this.velocidad = pk.velocidad;
    }

    
    
    public String getPok_id() {
        return Pok_id;
    }

    public void setPok_id(String Pok_id) {
        this.Pok_id = Pok_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo1() {
        return tipo1;
    }

    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }

    public String getTipo2() {
        return tipo2;
    }

    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getEnergiaAct() {
        return energiaAct;
    }

    public void setEnergiaAct(int energiaAct) {
        this.energiaAct = energiaAct;
    }

    public int getEnergiaMax() {
        return energiaMax;
    }

    public void setEnergiaMax(int energiaMax) {
        this.energiaMax = energiaMax;
    }

    public double getAtaque() {
        return ataque;
    }

    public void setAtaque(double ataque) {
        this.ataque = ataque;
    }

    public double getDefensa() {
        return defensa;
    }

    public void setDefensa(double defensa) {
        this.defensa = defensa;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public double getDefensaEsp() {
        return defensaEsp;
    }

    public void setDefensaEsp(double defensaEsp) {
        this.defensaEsp = defensaEsp;
    }

    public double getAtaqueEsp() {
        return ataqueEsp;
    }

    public void setAtaqueEsp(double ataqueEsp) {
        this.ataqueEsp = ataqueEsp;
    }
    /**
     * Compara si l'objecte pokemon es igual a la propia clase.
     * @param un objecte de la clase pokemon
     * @return True si l'objecte es igual a la clase pokemon, false en cas contrari.
     */
    @Override
    public boolean equals(Object pokemon){
        boolean b = false;
            if (pokemon == this) {
                b = true;
            }
        
        return b;
    }

/**
 * Sobrescriu la la funcio ja definida to string. Agafa tots els atributrs de 
 * l'objecte, els parseja en string i els concatena.
 * @return Retorna un string amb tota la informació dels atributs de la clase
 * concatenats.
 */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Pokemon: {Identificador: ");
        sb.append(this.Pok_id);
        sb.append(" || Nom: ");
        sb.append(this.nombre);
        sb.append(" || Tipus 1: ");
        sb.append(this.tipo1);
        sb.append(" || Tipus 2: ");
        sb.append(this.tipo2);
        sb.append(" || Nivell: ");
        sb.append(this.nivel);
        sb.append(" || Energia: ");
        sb.append(this.energiaAct);
        sb.append(" || Màxim-Energia: ");
        sb.append(this.energiaMax);
        sb.append(" || Atac: ");
        sb.append(this.ataque);
        sb.append(" || Defensa: ");
        sb.append(this.defensa);
        sb.append(" || Velocitat: ");
        sb.append(this.velocidad);
        sb.append(" || Defensa-Especial: ");
        sb.append(this.defensaEsp);
        sb.append(" || Atac-Especial: ");
        sb.append(this.ataqueEsp);
        sb.append(" } ");
        
        return sb.toString();
    }
    
    public static void main(String[] args) {
        
        Pokemon p1 = new Pokemon("001", "Altaria", "Psiquic", "Fuego", 100, 85, 150, 68, 75, 125.58, 80.56, 95.78);
        Pokemon p2 = new Pokemon("002", "Charizar", "Foc", "Drac", 120, 90, 130, 46, 78, 130.99, 78.66, 95.78);
        Pokemon p3 = new Pokemon("001", "Altaria", "Vent", "Fuego", 100, 85, 150, 68, 75, 125.58, 80.56, 95.78);
        Pokemon p4 = new Pokemon("001", "Meowth", "Psiquic", "Nose", 75, 80, 140, 79, 80, 145.57, 80, 95.123);
        Pokemon p5 = new Pokemon(p1);

        
        
        ArrayList <Pokemon> llistat = new ArrayList <Pokemon>();
        
        llistat.add(p1);
        llistat.add(p2);
        llistat.add(p3);
        llistat.add(p4);
        llistat.add(p5);
        
        for (Pokemon pokemon : llistat) {
            System.out.println(pokemon);
        }
        
        
        
    }
    
}
