/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ex3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Francisco Regaña Corral
 */
public class Validador {

    /**
     * Funcio que compara si l'estring camp esta buit o no.
     * @param camp L'string a analitzar
     * @return True si el camp es ple false si el camp es buit.
     */
    public boolean validaCampNoBuit(String camp) {
        boolean flag = (!camp.equals("") || camp != null);
        return flag;
    }

    /**
     * Funcio que comprova que el camp introduït no superi el tamany desitjat
     * @param camp String amb el camp.
     * @param tamanyMaxCamp Número de caràcters del camp en cuestio
     * @return True si supera el camp, false en cas contrari.
     */
    public boolean validaTamanyMaximCamp(String camp, int tamanyMaxCamp) {
        return camp.length() > tamanyMaxCamp;
    }

    /**
     * Camp que valida mitjançant expresions regulars si el camp email es
     * correcte o no.
     * @param email String el cual la funció comprovarà si l'email esta correctament escrit
     * o no.
     * @return True si el camp email es correcte. Fals en cas contrari
     */
    public boolean validaCampEmail(String email) {
        Pattern p = Pattern.compile("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$");
        Matcher m = p.matcher(email);
        return m.find();
    }
    /**
     * Funcio que valida si el camp de la web es correcte.
     * @param web String el cual sera examinat mitjançant regex si esta correctament
     * escrit en format web.
     * @return True si la web esta correctament escrita. fals en cas contrari.
     */
    public boolean validaWeb(String web){
        Pattern p = Pattern
                .compile("^((((https?|ftps?|gopher|telnet|nntp)://)|(mailto:|news:))" + 
                        "(%[0-9A-Fa-f]{2}|[-()_.!~*';/?:@&=+$, A-Za-z0-9])+)" + "([).!';/?:, ][[:blank:]])?$");
        Matcher m = p.matcher(web);
        return m.find();
    }

    /**
     * Mòdul que controla que a un comentari no hi hagi cap paraula malsonant.
     * @param paraules És el comentari contingut al formulari a examinar.
     * @return True si el llenguatge empleat es educat i correcte i False en cas
     * contrari
     */
    public boolean validaNoParaulesOfensives(String paraules) {
        boolean llenguatge_correcte = true;
        String[] palabrotas = {"ximple", "imbècil", "babau", "inútil", "burro",
            "loser", "noob", "capsigrany", "torrecollons", "fatxa", "nazi", "supremacista"};
        for (String palabrota : palabrotas) {
            //System.out.println(palabrota);
            if (paraules.contains(palabrota)) {
                llenguatge_correcte = false;
                break;
            }
        }
        return llenguatge_correcte;
    }
    
    /**
     * Paràmetre que valida si el nom està en mayuscules mitjançant un patró i
     * un matcher.
     * @param nom És el nom que volem comprobar si esta en mayúscules
     * @return True si el nom està en mayuscules i False en cas
     * contrari.
     */
    public boolean validaNomMayuscula(String nom) {

        String input = nom;
        Pattern p = Pattern.compile("^[A-Z][a-z]+$");
        Matcher m = p.matcher(input);
        return m.find();
    }
}
