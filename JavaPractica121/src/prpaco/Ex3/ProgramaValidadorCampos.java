package Ex3;



/**
 * 23. Crea una classe Validador que servirà per verificar camps de formularis;
 * i una classe Main que provi tots els mètodes. Aquests seran: 
 * -validaCampNoBuit(String camp) 
 * -validaTamanyMaximCamp(String camp, int tamanyMaxCamp) 
 * -validaCampEmail(String camp) 
 * - validaIntervalNumero(int intMinim, int intMaxim)
 * 
 * @author Francisco Regaña Corral
 */
public class ProgramaValidadorCampos {

    static final String SI = "Sí";
    static final String NO = "No";
    static final String CAMP_BUIT = "El camp està buit?";
    static final String CAMP_INTERNVAL = "El camp sobrepassa l'interval indicat?";
    static final String CAMP_EMAIL = "El camp és un email vàlid?";
    static final String NOM_MAYUSCULA = "El nom està en mayúscula el primer caracter?";
    static final String PARLA_BE = "Has utilitzat un llenguatge correcte i educat??";
    static final String WEB_VALIDA = "La web es valida?";
    
/**
 * Funcio que converteix un boleà en una resposta afirmativa si el booleà es cert,
 * o una resposta negtiva si el bollea es fals.
 * 
 * @param respostaAfirmativa És el boleá que analiltzarem.
 * @author Profe
 */
    public static void imprimeixResposta(boolean respostaAfirmativa) {
        if(respostaAfirmativa)
            System.out.println(SI);
        else
            System.out.println(NO);
    }
    
    public static void main(String[] args) {

        Validador validador = new Validador();
        
        System.out.println(CAMP_BUIT);
        imprimeixResposta(validador.validaCampNoBuit("Bones")); 
        System.out.println(CAMP_BUIT);
        imprimeixResposta(validador.validaCampNoBuit("")); 
        
        System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("Nombre",20));
        System.out.println(CAMP_INTERNVAL);
        imprimeixResposta(validador.validaTamanyMaximCamp("456789",5));
        
        System.out.println(CAMP_EMAIL);
        imprimeixResposta(validador.validaCampEmail("Nombre"));
        System.out.println(CAMP_EMAIL);
        imprimeixResposta(validador.validaCampEmail("amoros@gmail.com"));
        
        System.out.println(NOM_MAYUSCULA);
        imprimeixResposta(validador.validaNomMayuscula("Francisco"));
        System.out.println(NOM_MAYUSCULA);
        imprimeixResposta(validador.validaNomMayuscula("jordi"));
        
        System.out.println(PARLA_BE);
        imprimeixResposta(validador.validaNoParaulesOfensives("Tots som molt educats i no diem paraulotes"));
        System.out.println(PARLA_BE);
        imprimeixResposta(validador.validaNoParaulesOfensives("Em sembla que ets un ximple"));
        
        System.out.println(WEB_VALIDA);
        imprimeixResposta(validador.validaWeb("https://netbeans.apache.org/"));
        System.out.println(WEB_VALIDA);
        imprimeixResposta(validador.validaWeb("Hola hola hola"));
   }
}
