
package practica2;

import java.util.Objects;

/**
 * 
 * @author Andres Carmona Motta
 */


public class Ejercicio2 {
    
        
        
    private String pok_id;
    private String nombre;
    private String tipo1;
    private String tipo2;
    private int nivel;
    private int energiaAct;
    private int energiaMax;
    private double ataque;
    private double defensa;
    private double velocidad;
    private double defensaEsp;
    private double ataqueEsp;
       
    
    public Ejercicio2(String pok_id, String nombre, String tipo1, String tipo2, int nivel, int energiaAct, int energiaMax, double ataque, double defensa, double velocidad, double defensaEsp, double ataqueEsp) {
        this.pok_id = pok_id;
        this.nombre = nombre;
        this.tipo1 = tipo1;
        this.tipo2 = tipo2;
        this.nivel = nivel;
        this.energiaAct = energiaAct;
        this.energiaMax = energiaMax;
        this.ataque = ataque;
        this.defensa = defensa;
        this.velocidad = velocidad;
        this.defensaEsp = defensaEsp;
        this.ataqueEsp = ataqueEsp;
    }

    public String getPok_id() {
        return pok_id;
    }

    public void setPok_id(String pok_id) {
        this.pok_id = pok_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo1() {
        return tipo1;
    }

    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }

    public String getTipo2() {
        return tipo2;
    }

    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getEnergiaAct() {
        return energiaAct;
    }

    public void setEnergiaAct(int energiaAct) {
        this.energiaAct = energiaAct;
    }

    public int getEnergiaMax() {
        return energiaMax;
    }

    public void setEnergiaMax(int energiaMax) {
        this.energiaMax = energiaMax;
    }

    public double getAtaque() {
        return ataque;
    }

    public void setAtaque(double ataque) {
        this.ataque = ataque;
    }

    public double getDefensa() {
        return defensa;
    }

    public void setDefensa(double defensa) {
        this.defensa = defensa;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public double getDefensaEsp() {
        return defensaEsp;
    }

    public void setDefensaEsp(double defensaEsp) {
        this.defensaEsp = defensaEsp;
    }

    public double getAtaqueEsp() {
        return ataqueEsp;
    }

    public void setAtaqueEsp(double ataqueEsp) {
        this.ataqueEsp = ataqueEsp;
    }

    @Override
    public String toString() {
        
        StringBuilder sb = new StringBuilder();
        sb.append("Pokemon {Id=");
        sb.append(pok_id);
        sb.append(" nombre=");
        sb.append(nombre);
        sb.append(" nivel");
        sb.append(nivel);
        sb.append("}");
        return sb.toString();

        
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ejercicio2 other = (Ejercicio2) obj;
        if (this.nivel != other.nivel) {
            return false;
        }
        if (this.energiaAct != other.energiaAct) {
            return false;
        }
        if (this.energiaMax != other.energiaMax) {
            return false;
        }
        if (Double.doubleToLongBits(this.ataque) != Double.doubleToLongBits(other.ataque)) {
            return false;
        }
        if (Double.doubleToLongBits(this.defensa) != Double.doubleToLongBits(other.defensa)) {
            return false;
        }
        if (Double.doubleToLongBits(this.velocidad) != Double.doubleToLongBits(other.velocidad)) {
            return false;
        }
        if (Double.doubleToLongBits(this.defensaEsp) != Double.doubleToLongBits(other.defensaEsp)) {
            return false;
        }
        if (Double.doubleToLongBits(this.ataqueEsp) != Double.doubleToLongBits(other.ataqueEsp)) {
            return false;
        }
        if (!Objects.equals(this.pok_id, other.pok_id)) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.tipo1, other.tipo1)) {
            return false;
        }
        if (!Objects.equals(this.tipo2, other.tipo2)) {
            return false;
        }
        return true;
    }

    
    
}
