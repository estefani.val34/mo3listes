/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ValidadoresFormularios;

import java.util.Scanner;

public class Formulario {

    static final String SI = "true";
    static final String NO = "false";

    private static void verificacionCampo(boolean respuesta) {
        if (respuesta) {
            System.out.println("El campo es correcto");
        } else {
            System.out.println("El campo no es correcto");
        }
    }

    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);
        Validador validador = new Validador();

        String name = "Roy";
        String email = "lytthium01@gmail.com";
        String web = "www.yes.com";
        String comments = "Hola que ase.";

        
        System.out.println("Validar que cap dels camps de text requerits (inclòs el camp de comentaris) sigui correcte. ");
        verificacionCampo(validador.ValidarCampoVacio(name) && validador.ValidarEmail(email) && 
                validador.ValidarMaxCaractersData(name) && validador.ValidarMaxComment(comments) &&
                validador.ValidarWeb(web) && validador.ValidarTacos(comments));
       
        
       

    }

}
