
package prlaura.Ej4_UserComments;

import java.util.*;

public class User {
    private String name;
    private String email;
    private String web;
    private ArrayList<String> comments;

    //constructor1
    public User() {
    }

   //constructor2
    public User(String name, String email, String web, ArrayList<String> comments) {
        this.name = name;
        this.email = email;
        this.web = web;
        this.comments = comments;
    }

    //constructor3
    public User(String name, ArrayList<String> comments) {
        this.name = name;
        this.comments = comments;
    }

    //constructor4
    public User(String name) {
        this.name = name;
    }
    
    //getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public ArrayList<String> getComments() {
        return comments;
    }

    public void setComments(ArrayList<String> comments) {
        this.comments = comments;
    }

    //hash
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    //equals
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
}
