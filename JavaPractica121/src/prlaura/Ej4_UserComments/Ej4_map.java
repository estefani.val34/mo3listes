
package prlaura.Ej4_UserComments;

import java.util.*;

public class Ej4_map {
    
    private static final Map<String,User> USERS = new HashMap<String,User>();
    
    /**
     * This function creates new users with their comments
     * @return map users
     */
    private static Map<String,User> addUsers() {
        ArrayList<String> commentsLaura = new ArrayList<>();
        ArrayList<String> commentsDiego = new ArrayList<>();
        
        String comment1Laura = "Hola, me llamo Laura";
        String comment2Laura = "Tengo 29 años";
        String comment3Laura = "Vivo en Barcelona";
        String comment1Diego = "Hola, me llamo Diego";
        
        commentsLaura.add(comment1Laura);
        commentsLaura.add(comment2Laura);
        commentsLaura.add(comment3Laura);
        commentsDiego.add(comment1Diego);
        
        User laura = new User("Laura","laura@gmail.com","www.laura.com",commentsLaura);
        User diego = new User("Diego","diego@gmail.com","www.diego.com",commentsDiego);
        
        USERS.put("Laura",laura);
        USERS.put("Diego",diego);
        
        return USERS;
    }
    
    /**
     * This function prints the comments by one user
     */
    private static void showCommentsByUser(){
        Scanner myScan = new Scanner(System.in);
        System.out.print("Introduce a name: ");
        String nameIntro = myScan.nextLine();
        System.out.println("Comments by "+nameIntro);
        for (String name : USERS.keySet()) {
            String key = name;
            User myUser = USERS.get(key);
            if (myUser.getName().equals(nameIntro)){
                System.out.println(key+": "+myUser.getComments());
            }
        }
    }
    
    /**
     * This function prints all comments by all users
     */
    private static void showAllComments(){
        System.out.println("\nAll comments:");
        for (String name : USERS.keySet()) {
            String key = name;
            User myUser = USERS.get(key);
            System.out.println(key+": "+myUser.getComments());
        }
    }
    
    /**
     * This function runs the different functions
     * @param args 
     */
    public static void main(String[] args) {
        Map<String,User> users = addUsers();
        showCommentsByUser();
        showAllComments();
    }
}