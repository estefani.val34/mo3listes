
package prlaura.Ej2_Pokemons;

import java.util.*;

public class Ej2 {
    
    /**
     * This function creates 2 objects Pokemon and adds them to the arraylist pokemons
     * @return arraylist pokemons
     */
    public static ArrayList<Pokemon> createPokemons() {
        ArrayList<Pokemon> pokemons = new ArrayList<>();
        Pokemon pok1 = new Pokemon("Pok1","Picachu");
        Pokemon pok2 = new Pokemon("Pok2","Charmander","fuego",null,1,2,5,4,4,6,0,0);
        pokemons.add(pok1);
        pokemons.add(pok2);
        return pokemons;
    }

    /**
     * This function prints the arraylist pokemons
     * @param args 
     */
    public static void main(String[] args) {
        ArrayList<Pokemon> pokemons = createPokemons();
        for (Pokemon myPokemon : pokemons) {
            System.out.println(myPokemon.toString());
        }
    }
}
