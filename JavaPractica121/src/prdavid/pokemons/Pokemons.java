package pokemons;

import java.util.ArrayList;
import java.util.List;

/**
 * Class pokemons, contains the list of the pokemons and some methods
 * @author medel
 */
public class Pokemons {

    /**
     * Create the list of the pokemons
     * @return the list of pokemons
     */
    public static List<Pokemon> crearPokemons() {
        List<Pokemon> pokemons = new ArrayList<>();
        pokemons.add(new Pokemon("1","Bulbasaur","Planta","Veneno",5,60,80,60,60,60,60,60));
        pokemons.add(new Pokemon("756","Megakabuterimon","Bicho","Veneno",70,100,180,160,160,160,160,160));
        return pokemons;
    }
    
    /**
     * main of the project
     * calls the functions
     * @param args 
     */
    public static void main(String[] args) {
        List<Pokemon> pokemons = crearPokemons();
        for (Pokemon pokemon : pokemons) {
            System.out.println(pokemon.getNombre());
            System.out.println(pokemon.getTipo1());
            System.out.println(pokemon.getTipo2());
            System.out.println(pokemon.toString());
        }
        
    }
    
}

