package Validacion;

/**
 * 23. Crea una classe Validador que servirà per verificar camps de formularis;
 * i una classe Main que provi tots els mètodes. Aquests seran: 
 * -validaCampNoBuit(String camp) 
 * -validaTamanyMaximCamp(String camp, int tamanyMaxCamp) 
 * -validaCampEmail(String camp) 
 * - validaIntervalNumero(int intMinim, int intMaxim)
 */
public class ProvaValidador {

    static final String SI = "Sí";
    static final String NO = "No";
    static final String CAMP_NO_BUIT = "El camp està buit?-> ";
    static final String CAMP_INTERNVAL = "El camp sobrepassa l'interval indicat?-> ";
    static final String CAMP_EMAIL = "El camp és un email vàlid?-> ";
    static final String CAMP_WEB = "El camp és una url vàlida?-> ";
    static final String NOM_CORRECTE = "El nom es correcte?-> ";
    static final String FRASE_SENSE_PARAULOTES = "La frase conté alguna paraula malsonant?-> ";
    
    /**
     * Converteix un boolea en una resposta de Si o No que mostra 
     * per pantalla.
     * @param respostaAfirmativa 
     */
    private static void imprimeixResposta(boolean respostaAfirmativa) {
        if(respostaAfirmativa)
            System.out.println(SI);
        else
            System.out.println(NO);
    }
    
    /**
     * main of the project calls al the functions
     * @param args 
     */
    public static void main(String[] args) {

        ValidadorCampos validador = new ValidadorCampos();
        System.out.println(CAMP_NO_BUIT + " Bones");
        imprimeixResposta(validador.validaCampNoBuit("Bones")); 
        System.out.println(CAMP_NO_BUIT + "");
        imprimeixResposta(validador.validaCampNoBuit("")); 
        
        System.out.println(NOM_CORRECTE + " David");
        imprimeixResposta(validador.validateStartsWithCapital("David"));
        System.out.println(NOM_CORRECTE + " david");
        imprimeixResposta(validador.validateStartsWithCapital("david"));
        
        String txt0 = "David Medel Pizarro";
        String txt1 = "María del Rosario Cayetana Fitz-James Stuart y Silva";
        String txt2 = "Hola que tal com estas?";
        String txt3 = "Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años";
        System.out.println(CAMP_INTERNVAL + "(>40) " + txt0);
        imprimeixResposta(validador.validaTamanyMaximCamp(txt0,40));
        System.out.println(CAMP_INTERNVAL + "(>40) " + txt1);
        imprimeixResposta(validador.validaTamanyMaximCamp(txt1,40));
        System.out.println(CAMP_INTERNVAL + "(>140) " + txt2);
        imprimeixResposta(validador.validaTamanyMaximCamp(txt2,140));
        System.out.println(CAMP_INTERNVAL + "(>140) " + txt3);
        imprimeixResposta(validador.validaTamanyMaximCamp(txt3,140));
        
        System.out.println(CAMP_EMAIL + " Nombre");
        imprimeixResposta(validador.validaCampEmail("Nombre"));
        System.out.println(CAMP_EMAIL + " miquel.angel.amoros@gmail.com");
        imprimeixResposta(validador.validaCampEmail("miquel.angel.amoros@gmail.com"));
                
        System.out.println(CAMP_WEB + " https://www.proven.cat");
        imprimeixResposta(validador.validaCampDireccioWeb("https://www.proven.cat"));
        System.out.println(CAMP_WEB + " estonoesunaweb");
        imprimeixResposta(validador.validaCampDireccioWeb("estonoesunaweb"));
        
        System.out.println(FRASE_SENSE_PARAULOTES + " Ets un burro i un ximple");
        imprimeixResposta(validador.nonValidWords("Ets un burro i un ximple"));
        System.out.println(FRASE_SENSE_PARAULOTES + " hola maco que tal estas?");
        imprimeixResposta(validador.nonValidWords("hola maco que tal estas?"));
   }
  
}
