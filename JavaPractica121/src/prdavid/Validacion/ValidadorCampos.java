package Validacion;

/**
 * 23. Crea una classe Validador que servirà per verificar camps de formularis;
 * i una classe Main que provi tots els mètodes. Aquests seran: -
 * validaCampNoBuit(String camp) - validaTamanyMaximCamp(String camp, int
 * tamanyMaxCamp) - validaCampEmail(String camp) - validaIntervalNumero(int
 * intMinim, int intMaxim)
 */
public class ValidadorCampos {

    /**
     * constructor
     */
    public ValidadorCampos() {
    }

    /**
     * function that comprove if the input is void or not
     * @param camp
     * @return boolean
     */
    public boolean validaCampNoBuit(String camp) {
        return camp != null && camp.isEmpty(); // Otras opciones, camp.equals("");
    }

    /**
     * function that comprove you don't exceed the maximum of the field
     * @param camp
     * @param tamanyMaxCamp
     * @return boolean
     */
    public boolean validaTamanyMaximCamp(String camp, int tamanyMaxCamp) {
        return camp.length() > tamanyMaxCamp;
    }

    /**
     * function that check if the mail is valid or not
     * @param email
     * @return boolean
     */
    public boolean validaCampEmail(String email) {
        // https://www.tutorialspoint.com/validate-email-address-in-java 
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    /**
     * function that check if the url is valid or not
     * @param web
     * @return boolean
     */
    public boolean validaCampDireccioWeb(String web) {
        String regex = "https?:\\/\\/(www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)";
        return web.matches(regex);
    }

    /**
     * function that check that the String is a Name that start with capital letter
     * @param name
     * @return boolean
     */
    public boolean validateStartsWithCapital(String name) {
        String regex = "^[A-Z][a-z]+$";
        return name.matches(regex);
    }

    /**
     * function that check you don't introduce any invalid word
     * @param txt
     * @return boolean
     */
    public boolean nonValidWords(String txt) {
        String[] words = new String[]{"ximple", "imbècil", "babau", "inútil", "burro", "loser", "noob", "capsigrany", "torrecollons", "fatxa", "nazi", "supremacista"};
        boolean valid = true;
        for (int i = 0; i < words.length && valid; i++) {
            if (txt.contains(words[i])){
                valid = false;
            }
        }
        return valid;
    }
}
