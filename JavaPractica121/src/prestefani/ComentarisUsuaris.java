package practica22;

import java.util.ArrayList;
/**
 * 
 * @author estefani
 */
public class ComentarisUsuaris {

    private String nom;
    private String email;
    private String web;
    private String comentari;

    public ComentarisUsuaris() {
    }

    public ComentarisUsuaris(String nom) {
        this.nom = nom;
    }

    public ComentarisUsuaris(String nom, String email, String web, String comentari) {
        this.nom = nom;
        this.email = email;
        this.web = web;
        this.comentari = comentari;
    }

    public String getComentari() {
        return comentari;
    }

    public void setComentari(String comentari) {
        this.comentari = comentari;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @Override
    public String toString() {
        return "ComentarisUsuaris{" + "nom=" + nom + ", email=" + email + ", web=" + web + ", comentari=" + comentari + '}';
    }

    
    /**
     * listar comentarios
     * @param comentarisArrayList 
     */
    public static void listarComentarios(ArrayList<ComentarisUsuaris> comentarisArrayList) {
        for (ComentarisUsuaris c : comentarisArrayList) {
            System.out.println(c);
        }
    }
    /**
     * encontrar Comentario Por Nombre
     * @param nombre
     * @param comentarisArrayList 
     */
    public static void encontrarComentarioPorNombre(String nombre,ArrayList<ComentarisUsuaris> comentarisArrayList) {
        for (ComentarisUsuaris c : comentarisArrayList) {
            if (c.getNom().equals(nombre)) {
                System.out.println(c);
            }
        }
    }

}
