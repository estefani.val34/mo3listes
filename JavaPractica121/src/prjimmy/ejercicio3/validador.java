
package ejercicio3;

public class validador {

    public String verificador(String tipo, String palabra) {
//Creamos un array con todas las palabras que no se permitan
        String palabrotas[] = {"ximple","imbècil","babau","inútil","burro","loser","noob","capsigrany","torrecollons","fatxa","nazi","supremacista"};
        
        if (palabra.length() <= 0) {
            return ("ERROR-No puede quedar vacio el " + tipo);
        } else if (palabra.length() > 40 && !"comentario".equals(tipo)) {
            return ("ERROR-No puede sobrepasar los 40 carcateres en el " + tipo);
        } else if (palabra.length() >= 140 && tipo.equals("comentario")) {
            return ("ERROR-El comentario sobrepasa el limite de carcateres");
        }

        switch (tipo) {
            case "nombre":
                if (palabra.matches("^[A-Z][a-z]+[\\s]+[a-zA-Z]*$")) {
                    return ("El Nombre es correcto");
                } else {
                    return ("ERROR-Nombre el primero caracter en mayuscula e ingrese el nombre y apellido(Lucas Morales)");
                }
            case "email":
                if (palabra.matches("(\\W|^)[\\w.\\-]{0,25}@(yahoo|hotmail|gmail)\\.(com|es|org)(\\W|$)")) {
                    return ("El Email es correcto");
                } else {
                    return ("ERROR-El Email no tiene el formato requerizo");
                }
            case "sitioWeb":
                if (palabra.matches("^(?:http(s)?:\\/\\/)?[\\w.-]+(?:\\.[\\w\\.-]+.+)+$")) {
                    return "El sitio Web es correcto";
                } else {
                    return ("ERROR- El formato del sitio web es incorrecta");
                }
            case "comentario":             
                
                for (String palabrota : palabrotas) {
                    if(palabra.contains(palabrota)){
                        return "ERROR-Tu comenatario es ofensivo no puedes escribir "+palabrota;
                    }
                }
                return "Comentario Correcto";
                
            default:
                break;
        }
        return null;
    }
}
