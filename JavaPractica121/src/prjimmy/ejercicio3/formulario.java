package ejercicio3;

public class formulario {

    public static void main(String[] args) {
        validador metodos = new validador();
        /*Datos correctos prueba*/
        System.out.println("---------DATOS CORRECTOS-------------");
        String nombre = "Jimmy Daniel";
        System.out.println("El nombre: "+nombre);
        System.out.println(metodos.verificador("nombre", nombre));
        System.out.println("------");
        String email = "miquel.angel.amoros@gmail.com";
        System.out.println("El email es: "+email);
        System.out.println(metodos.verificador("email", email));
        System.out.println("------");
        String sitioWeb = "www.google.es";
        System.out.println("El sitio web es: "+sitioWeb);
        System.out.println(metodos.verificador("sitioWeb", sitioWeb));
        System.out.println("------");
        String comentario = "esto es un buen comentario";
         System.out.println("El comentario es: "+comentario);
        System.out.println(metodos.verificador("comentario", comentario));
        System.out.println("\n");
        /*Datos incorrectos prueba*/
             System.out.println("---------DATOS INCORRECTOS-------------");
        nombre = "jimmy daniel";
        System.out.println("El nombre: "+nombre);
        System.out.println(metodos.verificador("nombre", nombre));
        System.out.println("------");
        email = "miquel.angel.amoros@gmail.c";
        System.out.println("El email es: "+email);
        System.out.println(metodos.verificador("email", email));
        System.out.println("------");
        sitioWeb = "wwwgoogle";
        System.out.println("El sitio web es: "+sitioWeb);
        System.out.println(metodos.verificador("sitioWeb", sitioWeb));
        System.out.println("------");
        comentario = "eres un  burro";
         System.out.println("El comentario es: "+comentario);
        System.out.println(metodos.verificador("comentario", comentario));
    }
}
