package prsteven.practica_1_2;

/**
 *
 * @author stevenrolf
 */
public class Practica_1_2 {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Pokemon pokemon1 = new Pokemon("123", "Bulbasaur", "Grass", "Poison", 27, 110, 29, 70, 51, 20, 50, 50);
        Pokemon pokemon2 = new Pokemon("456", "Charizard", "Fire", "Flying", 50, 250, 79, 100, 81, 60, 100, 100);
        System.out.println(pokemon1);
        System.out.println(pokemon2);
        System.out.println("El tipo1 del pokemon1 es: " + pokemon1.getTipo1());

    }
}
