package prsteven.practica_1_2_1;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author stevenrolf
 * @version 1.0
 */

public class ejercicio3 {

//    @return Es el main principal y donde hay un menu de inicio para elegir el ejercicio
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(" ELIGE UNA OPCION " + "\n 1: ejercicio3"
                + "\n 2: ejercicio4");
        int Answer = sc.nextInt();
        switch (Answer) {
            case 1:
                ejercicio3();
                break;
            case 2:
                ejercicio4();
                break;
            default:
                System.out.println("Es necesario seleccionar una opcion");
        }
    }

//    @return En esta funcion se realizala el ejercicio 4 con todas las conditicones
    public static void ejercicio4() {
        System.out.println("\n");
        System.out.println("EJERCICIO 4");
        //@see Se realiza un string para crear los 4 usuarios con nom, mail, web, comentari

        ComentarisUsuari[] comentariList = new ComentarisUsuari[4];
        //@see Se crea idividualmente los usuarios untroduciendo por usuario

        comentariList[0] = new ComentarisUsuari("Steven", "Steven@mail.com", "Steven.com",
                "Darte cuenta de que los pequeños detalles son los que de verdad importan.");
        comentariList[1] = new ComentarisUsuari("Paco", "Paco@mail.com", "Paco.com",
                "Los momentos más sencillos son en realidad los más bonitos.");
        comentariList[2] = new ComentarisUsuari("Steven", "Steven@mail.com", "Steven.com",
                "Los amigos de verdad nunca se escapan.");
        comentariList[3] = new ComentarisUsuari("Paul", "Paul@mail.com", "Paul.com",
                "Merece la pena intentar lo que se te pase por la cabeza, porque al final nos arrepentimos más de lo que no hemos hecho que de nuestros errores.");

        //@see Se pide al usuario introducir el nombre ya registrado
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduce el nombre  de usuario");
        String name = sc.nextLine();
        //@see Se comprara con el nombre introducido para que busque en el string el nom e imprima los comentarios

        for (ComentarisUsuari comentariaUsuari : comentariList) {
            if (name.equals(comentariaUsuari.getNom())) {
                System.out.println(comentariaUsuari);
            }
        }
        //@see Se imprime todos los comentarios

        System.out.println("\n");
        for (ComentarisUsuari comentariaUsuari : comentariList) {
            System.out.println(comentariaUsuari.getComentari());
        }
        //@see Se realiza un HashMap creado la key como nombre y value los comentarios

        System.out.println("\n");
        Map<String, String> map = new HashMap<>();
        map.put("Steven", "Darte cuenta de que los pequeños detalles son los que de verdad importan.");
        map.put("Paco", "Los momentos más sencillos son en realidad los más bonitos.");
        map.put("Andres", "Los amigos de verdad nunca se escapan.");
        map.put("Paul", "Merece la pena intentar lo que se te pase por la cabeza, "
                + "porque al final nos arrepentimos más de lo que no hemos hecho que de nuestros errores.");

        //@see se realiza un bucle para que imprima todas las keys y values del HashMap
        Iterator<String> it = map.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            System.out.println("Clave: " + key + " -> Valor: " + map.get(key));
        }

    }
    //@return Se crea una funcion para crear el ejercicio 3

    public static void ejercicio3() {
        System.out.println("\n");
        Validator validar = new Validator();
        Scanner sc = new Scanner(System.in);

        //@see Se crea el primer campo para escribir el nombre en el campo
        System.out.println("ESCRIBE UN COMENTARIO");
        System.out.println("----------------------CAMP 1----------------------");
        System.out.println("Nombre (requerido)");

        String name = sc.nextLine();
        //@see Al introducir el nombre se verifica si es campo esta vacio o no, se crea otra clase valor llamando la funcion validarCamp

        if (!validar.validarCamp(name)) {
            imprimirError("El campo esta vacio, debe introducir los datos necesarios");
        }
        //@see Se valida si la primer caracter es mayuscula o no, sino se imprime un aviso

        if (!validar.validarMayuscula(name)) {
            imprimirError("El primer caracter debe de estar en mayuscula. (Name)");
        }
        //@see Se valida el tamaño del nombre que no supere los 40 caracteres

        if (!validar.validaSize(name)) {
            imprimirError("No debe superar mas de 40 caracteres");
        }
        System.out.println("----------------------CAMP 2----------------------");
        System.out.println("E-mail (no sera publicado)(requerido)");
        String email = sc.nextLine();
        //@see Se valida el campo que no este vacio

        if (!validar.validarCamp(email)) {
            imprimirError("El campo esta vacio, debe introducir los datos necesarios");
        }
        //@see Se valida el tamaño del nombre que no supere los 40 caracteres

        if (!validar.validaSize(email)) {
            imprimirError("No debe superar mas de 40 caracteres");
        }
        if (!validar.validarMail(email)) {
            System.out.println("El email ingresado no es válido. (name@mail.com)");
        }
        System.out.println("----------------------CAMP 3----------------------");
        System.out.println("Sitio Web");
        String web = sc.nextLine();
        //@see Se valida el campo que no este vacio

        if (!validar.validarCamp(web)) {
            imprimirError("El campo esta vacio, debe introducir los datos necesarios");
        }
        //@see Se valida el tamaño del nombre que no supere los 40 caracteres

        if (!validar.validaSize(web)) {
            imprimirError("No debe superar mas de 40 caracteres");
        }
        if (!validar.validarMail(web)) {
            System.out.println("La url dada no es válida. (https://www.dominio.com/)");
        }

        System.out.println("----------------------CAMP 4----------------------");

        System.out.println("Commentario");
        String comentario = sc.nextLine();
        //@see Se valida el campo que no este vacio

        if (!validar.validarCamp(comentario)) {
            imprimirError("El campo esta vacio, debe introducir los datos necesarios");
        }
        //@see Se valida el tamaño del nombre que no supere los 140 caracteres

        if (!validar.validaSize2(comentario)) {
            imprimirError("No debe superar mas de 140 caracteres");
        }

        /*
        @see Se crea un String de las palabras no permitidas, se crea un array y luego se 
         busca en el string de los comentarios para imprimir el aviso de encontrado
         */
        if (!validar.validarInsulto(comentario)) {
            imprimirError("No es permitido palabras inapropiadas");
        }

        System.out.println("----------------------CAMP 5----------------------");
        System.out.println("Recibir subsiguientes comentarios por correo (y/n)");
        //@see Se crea un escaner de un solo caracter

        char Answer = sc.next().charAt(0);
        //@see Se realiza un switch de opciones

        switch (Answer) {
            case 'n':
                System.out.println("Has elegido NO recibir comentarios por correo");
                break;
            case 'y':
                System.out.println("Has elegido Si recibir comentarios por correo");
                break;
            default:
                System.err.print("El parametro introducido no es correcto");
                break;
        }
    }
    //@return Se crea uncion para imprimir mensaje

    public static void imprimirError(String message) {
        System.out.println(message);
    }
}
