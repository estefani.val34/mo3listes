package pralbert.Validacion;

/** 
23. 
* Crea una classe Validador que servirà per verificar camps de formularis; 
* i una classe Main que provi tots els mètodes. Aquests seran:
    - validaCampNoBuit(String camp)
    - validaTamanyMaximCamp(String camp, int tamanyMaxCamp)
    - validaCampEmail(String camp)
    - validaIntervalNumero(int intMinim, int intMaxim)
*/
public class ValidadorCampos
{

  public ValidadorCampos() {
      
  }
  
  public boolean validaCampNoBuit(String camp) {
      String primeralletra=camp.substring(0,1);
      String refex = "^[A-Z][a-z]+$";
     return camp !=null && camp.isEmpty(); 
  }
  
  public boolean validaTamanyMaximCamp(String camp, int tamanyMaxCamp) {
     return camp.length()>tamanyMaxCamp;
  }
    
  public boolean validaCampEmail(String email) {
     String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
     return email.matches(regex);
  }
  public boolean validaIntervalNumero(int camp, int numMin, int numMax) {
     return true;
  }
    boolean validaPrimeraLletraMajuscules(String bones) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    boolean validaCampURL(String nombre) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

