package pralbert.Validacion;

/**
 * 23. Crea una classe Validador que servirà per verificar camps de formularis;
 * i una classe Main que provi tots els mètodes. Aquests seran:
 * -validaCampNoBuit(String camp) -validaTamanyMaximCamp(String camp, int
 * tamanyMaxCamp) -validaCampEmail(String camp) - validaIntervalNumero(int
 * intMinim, int intMaxim)
 */
public class Validador {

    static final String YES = "Sí";
    static final String NO = "No";
    static final String FIELD_NOT_EMPTY = "El camp està informat?";
    static final String FIELD_FIRST_CAPITAL = "Primera lletra majuscula ?";
    static final String INTERVAL_FIELD = "El camp sobrepassa l'interval indicat?";
    static final String EMAIL_FIELD = "El camp és un email vàlid?";
    static final String URL_FIELD = "El camp és una adreça web vàlida?";

    // Converteix un boolea en una resposta de Si o No que mostra 
    // per pantalla.
    private static void printAnswer(boolean respostaAfirmativa) {
        if (respostaAfirmativa) {
            System.out.println(YES);
        } else {
            System.out.println(NO);
        }
    }

    public static void main(String[] args) {

        ValidadorCampos validador = new ValidadorCampos();
        System.out.println(FIELD_NOT_EMPTY);
        printAnswer(validador.validaCampNoBuit("Bones"));
        System.out.println(FIELD_NOT_EMPTY);
        printAnswer(validador.validaCampNoBuit(""));
        System.out.println(FIELD_NOT_EMPTY);
        printAnswer(validador.validaCampNoBuit(null));

        System.out.println(FIELD_FIRST_CAPITAL);
        printAnswer(validador.validaPrimeraLletraMajuscules("Bones"));
        System.out.println(FIELD_FIRST_CAPITAL);
        printAnswer(validador.validaPrimeraLletraMajuscules("bones"));
        System.out.println(FIELD_FIRST_CAPITAL);
        printAnswer(validador.validaPrimeraLletraMajuscules("boNsES"));
        System.out.println(FIELD_FIRST_CAPITAL);
        printAnswer(validador.validaPrimeraLletraMajuscules(""));

        System.out.println(INTERVAL_FIELD);
        printAnswer(validador.validaTamanyMaximCamp("Nombre", 20));
        System.out.println(INTERVAL_FIELD);
        printAnswer(validador.validaTamanyMaximCamp("456789", 5));

        System.out.println(EMAIL_FIELD);
        printAnswer(validador.validaCampEmail("Nombre"));
        System.out.println(EMAIL_FIELD);
        printAnswer(validador.validaCampEmail("miquel.angel.amoros@gmail.com"));

        System.out.println(URL_FIELD + " Nombre");
        printAnswer(validador.validaCampURL("Nombre"));
        String website1 = "https://www.apache.org/";
        System.out.println(URL_FIELD + " " + website1);
        printAnswer(validador.validaCampURL(website1));
        String website2 = "eldiario.es";
        System.out.println(URL_FIELD + " " + website2);
        printAnswer(validador.validaCampURL(website2));

    }

    public boolean validateFirstLetterCapital(String camp) {
//     String primeraLletra = camp.substring(0,1);
//     String primeraLletraMajuscula = camp.substring(0,1).toUpperCase();
//     return primeraLletra.equals(primeraLletraMajuscula); // Otras opciones, camp.equals("");
        String regex = "^[A-Z][a-z]+$";
        return camp.matches(regex);
    }

    public boolean validatorMaxLenghtField(String camp, int tamanyMaxCamp) {
        return camp.length() > tamanyMaxCamp;
    }

    public boolean validateEmailField(String email) {
        // https://www.tutorialspoint.com/validate-email-address-in-java 
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    public boolean validateURLField(String email) {
        // https://www.tutorialspoint.com/validate-email-address-in-java 
        String regex = "(http://|https://)(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?([a-z]+)?";
        return email.matches(regex);
    }

    public boolean validateNumberInterval(int camp, int numMin, int numMax) {
        return true;
    }

}
