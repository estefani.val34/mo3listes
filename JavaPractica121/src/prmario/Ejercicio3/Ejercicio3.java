package Ejercicio3;

public class Ejercicio3 {

    static final String SI = "true";
    static final String NO = "false";

    private static void imprimeixResposta(boolean respostaAfirmativa) {
        if (respostaAfirmativa) {
            System.out.println("SI");
        } else {
            System.out.println("NO");
        }
    }

    public static void main(String[] args) {

        Validator validador = new Validator();
        String name = "Mario";
        String email = "marioalonsovalencia.12@gmail.com";
        String web = "www.marioalonso.com";
        String comments = "I like the web page so much";

        /**
         * Validador si algun camp esta buit
         */
        System.out.println("1 – Validar que cap dels camps de text requerits (inclòs el camp de comentaris) sigui correcte.");
        System.out.println("El camp està buit?");
        imprimeixResposta(validador.validaCamp(name));

        /**
         * Validador que el name comenci per majúcules
         */
        System.out.println("2 – Validar que el campo nombre empieza unicamente con una lletra en majúscules.");
        System.out.println("El camp comença per majúscules?");
        imprimeixResposta(validador.validaMayus(name));

        /**
         * Validador camp name, email i web no superin 40 caràcters
         */
        System.out.println("3 – Validar que els camps de texto 1,2 i 3 no superin els 40 caràcters de llarg.");
        System.out.println("Els tres textos superen els 40 caràcters?");
        System.out.print("Name: ");
        imprimeixResposta(validador.validaTamanyMaximCamp(name, 40));
        System.out.print("Email: ");
        imprimeixResposta(validador.validaTamanyMaximCamp(email, 40));
        System.out.print("Web: ");
        imprimeixResposta(validador.validaTamanyMaximCamp(web, 40));

        /**
         * Validador camp comentaris no superi els 140 caràcters
         */
        System.out.println("4 – Validar que el campo de texto 4 (comentaris), no superi els 140 caràcters de llarg.");
        System.out.println("El text 4 supera els 140 caràcters?");
        imprimeixResposta(validador.validaTamanyMaximCamp(comments, 140));

        /**
         * Validador email estigui composat per estructura
         * <nom_usuari>@<nom_serv_correu>.<dommini>
         */
        System.out.println("5 – Validar que l’email estigui composada d’una cadena de caràcters amb un format d’email vàlid. \n"
                + "<nom_usuari>@<nom_serv_correu>.<domini>");
        System.out.println("És correcte el camp email?");
        imprimeixResposta(validador.validaCampEmail(email));

        /**
         * Validador web es composi de dos o més punts i l'últim subtext ha de
         * ser un domini màxim 4 caràcters
         */
        System.out.println("6 – Validar que el lloc web es composi d’una cadena de text amb un text separat seguit d’un o més punts. L’últim subtext ha de ser un domini(tamany màxim 4 caràcters, contant la / )");
        System.out.println("És correcte el camp web?");
        imprimeixResposta(validador.validaURL(web));

        /**
         * Validador si el camp comments conté alguna paraulota donada
         */
        System.out.println("7 -  Validar que el camp 4 (comentaris) no contingui cap paraula d’una llista de paraules prohibides [“ximple”,”imbècil”,”babau”,”inútil”,”burro”,”loser”,”noob”,”capsigrany”,”torrecollons”,”fatxa”,”nazi”,”supremacista”], tant en minúscules com amb alguna o totes las lletres en majúscules.");
        System.out.println("L'apartat de comentaris conté alguna paraulota?");
        imprimeixResposta(validador.validaParaules(comments));

    }

}
