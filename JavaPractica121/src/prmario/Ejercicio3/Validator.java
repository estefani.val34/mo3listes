package Ejercicio3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Validator {

    /**
     * Validador si algun camp esta buit
     */
    /**
     * 
     * @param name
     * @return return name diferente null and name.isEmpty()
     */
    public boolean validaCamp(String name) {
        return name != null && name.isEmpty();
    }

    /**
     * Validador que el name comenci per majúcules
     */
    
    /**
     * 
     * @param name
     * @return name.matches(regex)
     */
    public boolean validaMayus(String name) {
        String regex = "^[A-Z]+[a-z]*";
        return name.matches(regex);
    }

    /**
     * Validador camp name, email i web no superin 40 caràcters 
     * Validador camp comentaris no superi els 140 caràcters
     */
    /**
     * 
     * @param camp
     * @param tamanyMaxCamp
     * @return return camp.length() major que tamanyMaxCamp
     */
    public boolean validaTamanyMaximCamp(String camp, int tamanyMaxCamp) {
        return camp.length() > tamanyMaxCamp;
    }

    /**
     * Validador email estigui composat per estructura
     * <nom_usuari>@<nom_serv_correu>.<dommini>
     */
    /**
     * 
     * @param email
     * @return email.matches(regex)
     */
    public boolean validaCampEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex); //Encaja
    }

    public boolean validaURL(String web) {
        String regex = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(\\/.*)?$";
        return web.matches(regex);
    }

    /**
     * ArrayList que contindrà totes les paraules ofensives
     */
    static List<String> palabrasOfensivas = new ArrayList<String>();

    /**
     * Validador si el camp comments conté alguna paraulota donada
     */
    public boolean validaParaules(String comments) {

        String[] palabrasComment = comments.split("\\W+");
        boolean comValido = true;
        String[] paraules = {"ximple", "imbècil", "babau", "inútil", "burro", "loser",
            "noob", "capsigrany", "torrecollons", "fatxa", "nazi", "supremacista"};
        Collections.addAll(palabrasOfensivas, paraules);

        for (String palabraComentario : palabrasComment) {
            if (palabrasOfensivas.contains(palabraComentario)) {
                comValido = false;
                break;
            }
        }
        if (!comValido) {
            return true;
        } else {
            return false;
        }
    }

}
