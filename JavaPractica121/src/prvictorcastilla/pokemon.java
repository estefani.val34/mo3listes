/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

/**
 *
 * @author tarda
 */
public class pokemon {
    
    private String Pok_id;
    private String nombre;
    private String tipo1;
    private String tipo2;
    private int nivel;
    private int energiaAct;
    private int energiaMax;
    private double ataque;
    private double defensa;
    private double velocidad;
    private double defensaEsp;
    private double ataqueEsp;

    public String getPok_id() {
        return Pok_id;
    }

    public void setPok_id(String Pok_id) {
        this.Pok_id = Pok_id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo1() {
        return tipo1;
    }

    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }

    public String getTipo2() {
        return tipo2;
    }

    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getEnergiaAct() {
        return energiaAct;
    }

    public void setEnergiaAct(int energiaAct) {
        this.energiaAct = energiaAct;
    }

    public int getEnergiaMax() {
        return energiaMax;
    }

    public void setEnergiaMax(int energiaMax) {
        this.energiaMax = energiaMax;
    }

    public double getAtaque() {
        return ataque;
    }

    public void setAtaque(double ataque) {
        this.ataque = ataque;
    }

    public double getDefensa() {
        return defensa;
    }

    public void setDefensa(double defensa) {
        this.defensa = defensa;
    }

    public double getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(double velocidad) {
        this.velocidad = velocidad;
    }

    public double getDefensaEsp() {
        return defensaEsp;
    }

    public void setDefensaEsp(double defensaEsp) {
        this.defensaEsp = defensaEsp;
    }

    public double getAtaqueEsp() {
        return ataqueEsp;
    }

    public void setAtaqueEsp(double ataqueEsp) {
        this.ataqueEsp = ataqueEsp;
    }
    
    
    

 public static void main(String[] args) {
        
   
   //Guardamos los datos
        pokemon charizard =new pokemon();
             
         
        charizard.setNombre("charizard");
        charizard.setTipo1("fuego");
        charizard.setTipo2("volador");
        charizard.setNivel(100);
        charizard.setEnergiaAct(600);
        charizard.setEnergiaMax(550);
        charizard.setAtaque(100);
        charizard.setDefensa(140);
        charizard.setVelocidad(120);
        charizard.setDefensaEsp(113);
        charizard.setAtaqueEsp(121);
        
        
        //Obtenemos los datos y los imprimimos
          String nombre=charizard.getNombre();
          String tipo1=charizard.getTipo1();
          String tipo2=charizard.getTipo2();
          int nivel=charizard.getNivel();
          int energiAct=charizard.getEnergiaAct();
          int energiaMax=charizard.getEnergiaMax(); 
                     
          System.out.println("nombre:"+nombre+ "Tipo1:" +tipo1+ "Tipo2:" +tipo2+ "nivel" + nivel+ "energiaAct" +charizard.getAtaque() + "Defensa:" + charizard.getDefensa()+ "Velocidad" + charizard.getVelocidad() + "defensaesp" + charizard.getDefensaEsp() +"ataque especial"+ charizard.getAtaqueEsp());
           
           
        
 
        
   
    
}



}