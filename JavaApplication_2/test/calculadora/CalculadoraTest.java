/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author portatil
 */
public class CalculadoraTest {
    static Calculadora calc;
    
    public CalculadoraTest() {
        
    }
    @BeforeClass
    public static void beforeClass() {
        System.out.println("beforeClass()");
        calc=new Calculadora();
    }
    @AfterClass
    public static void afterClass() {
        System.out.println("afterClass()");
        
    }
    
    
    @Before
    public void before() {
        System.out.println("before()");
        calc.clear();
    }
    @Test
    public void testAdd(){
        System.out.println("add()");
        int resultado=calc.add(2,3);
        int esperado=5;
        assertEquals(resultado,esperado);
        
    }
    @Test
    public void testAnsSum(){
        System.out.println("ans()");
        calc.add(2,3);
        int resultado=calc.ans();
        int esperado=5;
        assertEquals( resultado,esperado);
        
    }
    @Test (expected=ArithmeticException.class)
    public void testDiv(){
        System.out.println("div()");
        
        int resultado=calc.div(10,0);
        int esperado=2;
        assertEquals( resultado,esperado);
        
    }
    @After
    public void after() {
        System.out.println("after()");
        calc.clear();
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
